## HOW TO BUILD AND INSTALL THE PAY RATE APP

The pay rate application was written in **C#**. The interface was created in **XAML** and connected to the backend. To build this project, ypu have to

1. Design your interface in **XAML**
1. Use **MVVM** technique to join the backend to the frontend
1. Design your database in Sql server or any other DBMS of your choice.
1. Set your classes and connect to the database using connecting strings

### HOW TO INSTALL THE PAY RATE APP

This application can be install by opening the payRate application in the debug folder.

# LICENSE

## Copyright _2021_ GIDEON OTENG

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.


