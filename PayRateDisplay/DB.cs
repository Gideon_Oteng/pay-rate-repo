﻿//Vaneesh Jha, Gideon Oteng and Prabh Singh
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PayRateDisplay
{
    internal class DB
    {
        const string CONNECTION_STRING = @"Server=.\SQL2019;Database=Personnel;Trusted_Connection=True;";
        const string DISPLAY_TABLE = "SELECT employeeID, name, position, hourlypayrate FROM Employee";
        const string MAX_PAY = "select max(hourlypayrate) as hourlypayrate from Employee";
        const string MIN_PAY = "select min(hourlypayrate) as hourlypayrate from Employee";

        private static DB db;
        public static DB Instance { get { db ??= new DB(); return db; } }

        private SqlConnection conn; 

        private DB()
        {
            conn = new SqlConnection(CONNECTION_STRING);
            conn.Open();
        }

        public List<PayRate> GetData()
        {
            List<PayRate> payrates = new List<PayRate>();

            using SqlCommand cmd = new SqlCommand(DISPLAY_TABLE, conn);
            using SqlDataReader dr = cmd.ExecuteReader();

            while (dr.Read())
                payrates.Add(new PayRate
                {
                    EmployeeID = dr.GetInt32(dr.GetOrdinal("employeeID")),
                    Name = dr.GetString(dr.GetOrdinal("name")),
                    Position = dr.GetString(dr.GetOrdinal("position")),
                    HourlyPayRate = dr.GetDecimal(dr.GetOrdinal("hourlypayrate"))
                });
            dr.Close();
            return payrates;
        }

        public decimal MaxPay()
        {
            decimal maxPay;
            using SqlCommand cmd = new SqlCommand(MAX_PAY, conn);
            using SqlDataReader dr = cmd.ExecuteReader();
            maxPay = dr.GetDecimal(dr.GetOrdinal("hourlypayrate"));
            Console.WriteLine(maxPay);
            return maxPay;
        }
    }
}
