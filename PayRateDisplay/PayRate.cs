﻿//Vaneesh Jha, Gideon Oteng and Prabh Singh
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PayRateDisplay
{
    internal class PayRate
    {
        public string Number { get; set; } = string.Empty;
        public int EmployeeID { get; set; }
        public string Name { get; set; } = string.Empty;
        public string Position { get; set; } = string.Empty;
        public double HourlyPayRate { get; set; }

    }
}
