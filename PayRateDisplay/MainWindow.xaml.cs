﻿//Vaneesh Jha, Gideon Oteng and Prabh Singh
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PayRateDisplay
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        DB db;
        VM vm;
        public MainWindow()
        {
            InitializeComponent();
            vm = VM.Instance;
            DataContext = vm;
            db = DB.Instance;
        }

        private void HighestPay_Click(object sender, RoutedEventArgs e)
        {
            outDisplay.Content = db.MaxPay().ToString();
        }

        private void LowestPay_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
