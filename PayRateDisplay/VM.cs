﻿//Vaneesh Jha, Gideon Oteng and Prabh Singh
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace PayRateDisplay
{
    class VM : INotifyPropertyChanged
    {
        DB db = DB.Instance;
        List<PayRate> payrates;

        #region Singleton
        private static VM vm;
        public static VM Instance
        {
            get
            {
                vm ??= new VM();
                return vm;
            }
        }
        #endregion

        private VM()
        {
            payrates = db.GetData();
            updatePayRate();
        }
        #region Properties
        public BindingList<PayRate> PayRates { get; set; } = new BindingList<PayRate>();

        #endregion
        private void updatePayRate()
        {
            payrates = payrates.OrderBy(p => p.EmployeeID).ToList();
            PayRates.Clear();
            foreach (PayRate p in payrates)
                PayRates.Add(p);
        }

        #region Property Change
        public event PropertyChangedEventHandler PropertyChanged;

        private void notifyChange([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
